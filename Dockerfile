
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9


WORKDIR /code


COPY ./requirements.txt /code/requirements.txt


RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt


COPY ./main.py /code/
COPY ./pred_views_model.py /code/


EXPOSE 8000


#ENTRYPOINT ["uvicorn", "main:app --reload"]


CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
