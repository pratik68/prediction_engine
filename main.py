from fastapi import FastAPI
import uvicorn
# import fasttext
from pred_views_model import pred_views_tf
import datetime
# import tensorflow as tf
# import pandas as pd
# from joblib import dump, load
# import numpy as np



    
app = FastAPI()
# model = fasttext.load_model("/Users/cb/Downloads/model.bin")
# ensemble = list()
# for i in range(20):
#     ensemble.append(tf.keras.models.load_model('/Users/cb/Downloads/az_ws2/saved_model/my_model_{}'.format(i)))

# print(ensemble)
# print('TF models are loaded')


@app.post("/predicted-views/")
def prediction(data: pred_views_tf):
    data = data.dict()
    #txt = data['txt']
    #return model[txt]

    # # load features from flat file
    # df = pd.read_csv('/Users/cb/Downloads/trainset3.csv')
    # df = df[['time_to_read',    'frst_bck_mean_avg_vus',    'sec_bck_mean_avg_vus', 'thrd_bck_mean_avg_vus',    'frth_bck_mean_avg_vus',    'ffth_bck_mean_avg_vus',    'sxth_bck_mean_avg_vus',    'mean_v1',  'mean_v2',  'mean_v3',  'mean_v4',  'mean_v5',  'frst_bck_mean_no_arts',    'sec_bck_mean_no_arts', 'thrd_bck_mean_no_arts',    'frth_bck_mean_no_arts',    'ffth_bck_mean_no_arts',    'sxth_bck_mean_no_arts',    'frst_bck_art_tag_cos_avg_vus', 'sec_bck_art_tag_cos_avg_vus',  'thrd_bck_art_tag_cos_avg_vus', 'frth_bck_art_tag_cos_avg_vus', 'ffth_bck_art_tag_cos_avg_vus', 'sxth_bck_art_tag_cos_avg_vus', 'art_tag_cos_v1',   'art_tag_cos_v2',   'art_tag_cos_v3',   'art_tag_cos_v4',   'art_tag_cos_v5',   'frst_bck_art_tag_cos_no_arts', 'sec_bck_art_tag_cos_no_arts',  'thrd_bck_art_tag_cos_no_arts', 'frth_bck_art_tag_cos_no_arts', 'ffth_bck_art_tag_cos_no_arts', 'sxth_bck_art_tag_cos_no_arts', 'frst_bck_tit_cos_avg_vus', 'sec_bck_tit_cos_avg_vus',  'thrd_bck_tit_cos_avg_vus', 'frth_bck_tit_cos_avg_vus', 'ffth_bck_tit_cos_avg_vus', 'sxth_bck_tit_cos_avg_vus', 'tit_cos_v1',   'tit_cos_v2',   'tit_cos_v3',   'tit_cos_v4',   'tit_cos_v5',   'frst_bck_tit_cos_no_arts', 'sec_bck_tit_cos_no_arts',  'thrd_bck_tit_cos_no_arts', 'frth_bck_tit_cos_no_arts', 'ffth_bck_tit_cos_no_arts', 'sxth_bck_tit_cos_no_arts', 'frst_bck_art_vec_cos_avg_vus', 'sec_bck_art_vec_cos_avg_vus',  'thrd_bck_art_vec_cos_avg_vus', 'frth_bck_art_vec_cos_avg_vus', 'ffth_bck_art_vec_cos_avg_vus', 'sxth_bck_art_vec_cos_avg_vus', 'art_vec_cos_v1',   'art_vec_cos_v2',   'art_vec_cos_v3',   'art_vec_cos_v4',   'art_vec_cos_v5',   'frst_bck_art_vec_cos_no_arts', 'sec_bck_art_vec_cos_no_arts',  'thrd_bck_art_vec_cos_no_arts', 'frth_bck_art_vec_cos_no_arts', 'ffth_bck_art_vec_cos_no_arts', 'sxth_bck_art_vec_cos_no_arts', 'frst_bck_authNam_vec_cos_avg_vus', 'sec_bck_authNam_vec_cos_avg_vus',  'thrd_bck_authNam_vec_cos_avg_vus', 'frth_bck_authNam_vec_cos_avg_vus', 'ffth_bck_authNam_vec_cos_avg_vus', 'sxth_bck_authNam_vec_cos_avg_vus', 'authNam_vec_cos_v1',   'authNam_vec_cos_v2',   'authNam_vec_cos_v3',   'authNam_vec_cos_v4',   'authNam_vec_cos_v5',   'frst_bck_authNam_vec_cos_no_arts', 'sec_bck_authNam_vec_cos_no_arts',  'thrd_bck_authNam_vec_cos_no_arts', 'frth_bck_authNam_vec_cos_no_arts', 'ffth_bck_authNam_vec_cos_no_arts', 'sxth_bck_authNam_vec_cos_no_arts', 'frst_bck_secNam_vec_cos_avg_vus',  'sec_bck_secNam_vec_cos_avg_vus',   'thrd_bck_secNam_vec_cos_avg_vus',  'frth_bck_secNam_vec_cos_avg_vus',  'ffth_bck_secNam_vec_cos_avg_vus',  'sxth_bck_secNam_vec_cos_avg_vus',  'secNam_vec_cos_v1',    'secNam_vec_cos_v2',    'secNam_vec_cos_v3',    'secNam_vec_cos_v4',    'secNam_vec_cos_v5',    'frst_bck_secNam_vec_cos_no_arts',  'sec_bck_secNam_vec_cos_no_arts',   'thrd_bck_secNam_vec_cos_no_arts',  'frth_bck_secNam_vec_cos_no_arts',  'ffth_bck_secNam_vec_cos_no_arts',  'sxth_bck_secNam_vec_cos_no_arts',  'mon',  'tues', 'wed',  'thurs',    'fri',  'sat',  'sun',  'lte_nt',   'ear_mr',   'mrng', 'lte_mr',   'aftn', 'lte_af',   'eve',  'ngth']]
    # dataset = df.values
    # X = dataset[:1]
    
    # # scale the features
    # sc = load('/Users/cb/Downloads/az_ws2/std_scaler.bin')
    # X_scaled = sc.transform(X)

    # # make predictions
    # yhat = [model.predict(data['scaled'].reshape(1,118), verbose=0) for model in ensemble]
    # yhat = np.array(yhat)
    # # calculate 95% gaussian prediction interval
    # interval = 1.96 * yhat.std()
    # lower, upper = yhat.mean() - interval, yhat.mean() + interval
    # mean = yhat.mean()

    return {
     'articleTitle': data['articleTitle'],
     'lower': 30,
     'prediction': 100,
     'upper': 40,
     'timestamp': datetime.datetime.now()
    }

    # except Exception as e:
    #     print(e)


if __name__ == '__main__':

    uvicorn.run(app, host= '0.0.0.0', port =8000)
    

