from pydantic import BaseModel
from typing import Optional

class pred_views_tf(BaseModel):
    articleTitle: str
    articeText: Optional[str] = None
    authorName: Optional[str] = None
    timestamp: Optional[str] = None
    sectionName: Optional[str] =  None
    tags: Optional[str] = None